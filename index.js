(function() {
    'use strict';

    var request = require('request'),
        cheerio = require('cheerio'),
        fs = require('fs'),
        url = 'http://www.lolesports.ru/lcl/lcl_2016_spring/schedule';

    function getFileName(path, extension) {
        var _path = path || false,
            _extension = extension || false,
            date = new Date(),
            filename = '';

        if (_path) {
            filename += _path;
        }

        filename += 'result_' + date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);

        if (_extension) {
            filename += '.' + _extension;
        } else {
            filename += '.json';
        }

        return filename;
    }

    function saveResultInFile() {
        fs.appendFile(getFileName('result/', 'json'), JSON.stringify(scheduleStack), function (error) {
            if (error) {
                throw error;
            }

            console.log('The "data to append" was appended to file!');
        });
    }

    request(url, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            var $ = cheerio.load(body),
                scheduleStack = [];

            $('.p-schedule_content > .p-schedule_item_day-title').each(function() {
                var playDate = '',
                    nextElem = this.next,
                    sheduleDayStack = [],
                    i, len;

                for (i = 0, len = this.children.length; i < len; i++) {
                    if ('text' === this.children[i].type) {
                        playDate = this.children[i].data;

                        break;
                    }
                }

                do {
                    var winner = null,
                        firstTeam = '',
                        secondTeam = '',
                        time = '';

                    time = nextElem.children[0].children[0].data;
                    firstTeam = nextElem.children[1].children[0].children[0].children[0].data;
                    secondTeam = nextElem.children[5].children[0].children[0].children[0].data;

                    if (undefined !== nextElem.children[1].children[2]) {
                        if ('p-schedule_result--victory' === nextElem.children[1].children[2].attribs.class) {
                            winner = firstTeam;
                        } else if ('p-schedule_result--victory' === nextElem.children[5].children[1].attribs.class) {
                            winner = secondTeam;
                        }
                    }

                    sheduleDayStack.push({
                        time: time,
                        teams: [firstTeam, secondTeam],
                        winner: winner
                    });

                    if (null === nextElem.next) {
                        break;
                    } else {
                        nextElem = nextElem.next;
                    }
                } while ('p' !== nextElem.name);

                scheduleStack.push({
                    date: playDate,
                    games: sheduleDayStack
                });
            });
        }
    });
})();